﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch_Com : MonoBehaviour
{
    public GameObject lightObject;
    public AudioSource audioSource;
    public Animation anim;
    public bool lightOn = false;
    private Light lightComp;
    public TriggerListener_Com trigger;
    public Image cursorImage;

	// Use this for initialization
	void Start ()
    {
        audioSource = this.GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
        lightComp = lightObject.GetComponent<Light>();
        cursorImage.enabled = false;
        if (lightOn == true)
        {
            lightComp.intensity = 1f;
        }
        else
        {
            lightComp.intensity = 0f;
        }
	}
	
    void OnMouseOver()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseExit()
    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        //toggles switch when player interacts with it
        if (trigger.playerEntered == true)
        {
            audioSource.Stop();
            audioSource.Play();
            anim.Stop();
            anim.Play();
            if (lightOn == true)
            {
                lightComp.intensity = 0f;
                lightOn = false;
            }
            else
            {
                lightComp.intensity = 1f;
                lightOn = true;
            }
        }
    }
}
